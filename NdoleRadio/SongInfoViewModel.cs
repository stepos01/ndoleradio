﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using NdoleRadio.Annotations;
using NdoleRadio.Model;
using Newtonsoft.Json;

namespace NdoleRadio
{
    public class SongInfoViewModel : INotifyPropertyChanged
    {

        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value; OnPropertyChanged();
                if (string.IsNullOrEmpty(value))
                {
                    BorderBrush = new SolidColorBrush(Colors.Transparent);
                }
                else
                {
                    BorderBrush = new SolidColorBrush(Colors.White);
                }
            }
        }
        private string _artist;

        public string Artist
        {
            get { return _artist; }
            set { _artist = value; OnPropertyChanged(); }
        }

        private string _cover;

        public string Cover
        {
            get { return _cover; }
            set { _cover = value; OnPropertyChanged(); }
        }
        private SolidColorBrush _borderBrush;

        public SolidColorBrush BorderBrush
        {
            get { return _borderBrush; }
            set { _borderBrush = value;OnPropertyChanged(); }
        }
        


        private DispatcherTimer _dispatcherTimer;
        public SongInfoViewModel()
        {
            _dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            _dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 30);
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                try
                {
                    var client = new HttpClient();
                    string result = client.GetStringAsync("https://www.radioking.fr/api/radio/214/track/live?cache=" + Guid.NewGuid().ToString()).Result;
                    if (!string.IsNullOrEmpty(result))
                    {
                        var data = JsonConvert.DeserializeObject<SongModelWrapper>(result);
                        if (data.Status == "success")
                        {
                            Deployment.Current.Dispatcher.BeginInvoke(() =>
                            {

                                _dispatcherTimer.Stop();
                                DateTime starTime;
                                var success = DateTime.TryParse(data.Data.Startdate, null, DateTimeStyles.None, out starTime);
                                if (success)
                                {
                                    var endTime = starTime;
                                    if (!data.Data.Title.ToLower().Contains("radio") && !data.Data.Title.ToLower().Contains("vn")) endTime = starTime.AddMinutes(3);
                                    Debug.WriteLine("endTime: " + endTime.ToShortTimeString());
                                    if (endTime.CompareTo(DateTime.Now) <= 0)
                                    {
                                        _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 5);
                                    }
                                    else
                                    {
                                        var span = endTime.Subtract(DateTime.Now);
                                        Debug.WriteLine("span: " + span.TotalSeconds);
                                        _dispatcherTimer.Interval = span;
                                    }
                                    _dispatcherTimer.Start();
                                    UpdateAlbumData(data.Data);
                                }
                            });


                        }

                    }
                }
                catch (Exception f)
                {
                    Debug.WriteLine(f);
                }


            });

        }

        private void UpdateAlbumData(SongModel data)
        {
            if(data.Title==Title)return;
            Title = data.Title;
            Artist = data.Artist;
            
            string url = "https://www.radioking.fr/api/track/cover/";
            if (!string.IsNullOrEmpty(data.Cover))
            {
                Cover = url + data.Cover;
            }
            else
            {
                Cover = "https://pbs.twimg.com/profile_images/565160694149304320/Zma9X_4F.jpeg";

            }
        }

        public void StartDataLoader()
        {
            _dispatcherTimer.Start();
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 30);
            dispatcherTimer_Tick(null, null);
        }

        public void StopDataLoader()
        {
            _dispatcherTimer.Stop();
        }





        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Reset()
        {
            Title = null;
            Artist = null;
            Cover = null;
        }
    }
}
