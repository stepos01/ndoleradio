﻿// -----------------------------------------------------------------------
//  <copyright file="MainPage.xaml.cs" company="Henric Jungheim">
//  Copyright (c) 2012-2014.
//  <author>Henric Jungheim</author>
//  </copyright>
// -----------------------------------------------------------------------
// Copyright (c) 2012-2014 Henric Jungheim <software@henric.org>
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Threading;
using Microsoft.Phone.BackgroundAudio;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TweetSharp;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace NdoleRadio
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Timer for updating the UI
        private const int _offsetKnob = 5;
        // Indexes into the array of ApplicationBar.Buttons
        //const int PrevButtonIndex = 0;
        const int PlayButtonIndex = 0;
        const int PauseButtonIndex = 1;
        //const int NextButtonIndex = 3;
        //readonly ApplicationBarIconButton _nextButton;
        ApplicationBarIconButton _pauseButton;
        ApplicationBarIconButton _playButton;
        //readonly ApplicationBarIconButton _prevButton;
        DispatcherTimer _timer;
        private TwitterViewModel _viewModel;
        private SongInfoViewModel _songInfo;
        private Popup loginPopup;
        public MainPage()
        {
            InitializeComponent();
            _viewModel = new TwitterViewModel();
            _songInfo = new SongInfoViewModel();
            Pivot.SelectedIndex = 0;

            //   _prevButton = ((ApplicationBarIconButton)(ApplicationBar.Buttons[PrevButtonIndex]));
            //_nextButton = ((ApplicationBarIconButton)(ApplicationBar.Buttons[NextButtonIndex]));
            TweetItem.DataContext = _viewModel;
            RadioItem.DataContext = _songInfo;
        }

        void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            _pauseButton = ((ApplicationBarIconButton)(ApplicationBar.Buttons[PauseButtonIndex]));
            _playButton = ((ApplicationBarIconButton)(ApplicationBar.Buttons[PlayButtonIndex]));

            // Initialize a timer to update the UI every half-second.
            _timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(0.5)
            };

            _timer.Tick += UpdateState;

            BackgroundAudioPlayer.Instance.PlayStateChanged += Instance_PlayStateChanged;
            Instance_PlayStateChanged(null, null);

            SetUpTwitterFeed();
        }

        private void LoadTweets(bool isRefresh = false)
        {
            _viewModel.LoadTweets((success) => Dispatcher.BeginInvoke(() =>
            {
                _viewModel.IsLoading = false;
                if (!success) MessageBox.Show("Could not retrieve Ndole tweets!");

            }), isRefresh);

        }

        private void SearchTweet(string query)
        {
            _viewModel.SearchTweet(query,(success) => Dispatcher.BeginInvoke(() =>
            {
                _viewModel.IsLoading = false;
                if (!success) MessageBox.Show("Could not retrieve Ndole tweets!");


            }));
        }
        private void SetUpTwitterFeed()
        {
            tweetList.ItemRealized += (o, args) =>
            {
                if (!_viewModel.IsLoading && tweetList.ItemsSource != null &&
                tweetList.ItemsSource.Count
                >= _offsetKnob)
                {
                    if (args.ItemKind == LongListSelectorItemKind.Item)
                    {
                        if ((args.Container.Content as
                         TwitterStatus).Equals(tweetList.
                         ItemsSource[tweetList.ItemsSource.Count - _offsetKnob]))
                        {
                            LoadTweets();
                        }
                    }
                }
            };

            var progressIndicator = SystemTray.ProgressIndicator;
            if (progressIndicator != null)
            {
                return;
            }

            progressIndicator = new ProgressIndicator();

            SystemTray.SetProgressIndicator(this, progressIndicator);

            var binding = new Binding("IsLoading") { Source = _viewModel };
            BindingOperations.SetBinding(
                progressIndicator, ProgressIndicator.IsVisibleProperty, binding);
            var textBinding = new Binding("ProgressText") { Source = _viewModel };
            BindingOperations.SetBinding(
                progressIndicator, ProgressIndicator.IsIndeterminateProperty, binding);
            BindingOperations.SetBinding(progressIndicator, ProgressIndicator.IsVisibleProperty, binding);
            BindingOperations.SetBinding(progressIndicator, ProgressIndicator.TextProperty, textBinding);

        }

        /// <summary>
        ///     PlayStateChanged event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Instance_PlayStateChanged(object sender, EventArgs e)
        {
            switch (BackgroundAudioPlayer.Instance.PlayerState)
            {
                case PlayState.Playing:
                    // Update the UI.
                    {
                        var track = BackgroundAudioPlayer.Instance.Track;
                        //txtTrack.Text = "Live from Ndole Land";
                        loader.Visibility = Visibility.Collapsed;
                        _songInfo.StartDataLoader();
                        //if (null != track)
                        //{
                        //    var duration = track.Duration;

                        //    if (duration > TimeSpan.Zero)
                        //    {
                        //        positionIndicator.IsIndeterminate = false;
                        //        positionIndicator.Maximum = duration.TotalSeconds;
                        //    }
                        //}
                    }

                    _playButton.IsEnabled = false;
                    _pauseButton.IsEnabled = true;

                    UpdateState(null, null);

                    // Start the timer for updating the UI.
                    _timer.Start();

                    break;
                case PlayState.Stopped:
                case PlayState.Paused:
                    // Update the UI.
                    loader.Visibility = Visibility.Collapsed;
                    _playButton.IsEnabled = true;
                    _pauseButton.IsEnabled = false;
                    _songInfo.StopDataLoader();
                    UpdateState(null, null);

                    // Stop the timer for updating the UI.
                    _timer.Stop();

                    break;
                case PlayState.Unknown:
                    _playButton.IsEnabled = true;
                    _pauseButton.IsEnabled = true;
                    loader.Visibility = Visibility.Collapsed;

                    break;
                case PlayState.BufferingStarted:
                    {
                        loader.Visibility = Visibility.Visible;
                        break;

                    }
            }

            //_nextButton.IsEnabled = true;
            //_prevButton.IsEnabled = true;
        }

        /// <summary>
        ///     Updates the status indicators including the State, Track title,
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void UpdateState(object sender, EventArgs e)
        {
            try
            {
                var player = BackgroundAudioPlayer.Instance;

                if (null == player)
                    return;

                //txtState.Text = string.Format("State: {0}", player.PlayerState);

                var track = player.Track;

                if (null != track)
                    //txtTrack.Text = string.Format("Track: {0}", track.Title);

                    // Set the current position on the ProgressBar.
                    //positionIndicator.Value = player.Position.TotalSeconds;

                // Update the current playback position.
                //var position = player.Position;
                //textPosition.Text = string.Format("{0:d2}:{1:d2}:{2:d2}", position.Hours, position.Minutes, position.Seconds);

                // Update the time remaining digits.
                if (null != track)
                {
                    //var timeRemaining = track.Duration - position;
                    //textRemaining.Text = string.Format("-{0:d2}:{1:d2}:{2:d2}", timeRemaining.Hours, timeRemaining.Minutes, timeRemaining.Seconds);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("MainPage.UpdateState() failed: " + ex.Message);
            }
        }

        /// <summary>
        ///     Click handler for the Skip Previous button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void prevButton_Click(object sender, EventArgs e)
        {
            // Show the indeterminate progress bar.
            //positionIndicator.IsIndeterminate = true;

            // Disable the button so the user can't click it multiple times before 
            // the background audio agent is able to handle their request.
            //_prevButton.IsEnabled = false;

            // Tell the background audio agent to skip to the previous track.
            //BackgroundAudioPlayer.Instance.SkipPrevious();
        }

        /// <summary>
        ///     Click handler for the Play button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void playButton_Click(object sender, EventArgs e)
        {
            // Tell the background audio agent to play the current track.
            BackgroundAudioPlayer.Instance.Play();
            loader.LoadingText = "Buferring...";
            loader.Visibility = Visibility.Visible;
        }

        /// <summary>
        ///     Click handler for the Pause button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void pauseButton_Click(object sender, EventArgs e)
        {
            // Tell the background audio agent to pause the current track.
            BackgroundAudioPlayer.Instance.Pause();
            loader.Visibility = Visibility.Collapsed;
            _songInfo.StopDataLoader();
            _songInfo.Reset();

        }

        /// <summary>
        ///     Click handler for the Skip Next button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void nextButton_Click(object sender, EventArgs e)
        {
            // Show the indeterminate progress bar.
            //positionIndicator.IsIndeterminate = true;

            // Disable the button so the user can't click it multiple times before 
            // the background audio agent is able to handle their request.
            //_nextButton.IsEnabled = false;

            // Tell the background audio agent to skip to the next track.
            //BackgroundAudioPlayer.Instance.SkipNext();
        }
        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((Pivot)sender).SelectedIndex)
            {
                case 0:
                    ApplicationBar = ((ApplicationBar)Resources["AppBar1"]);
                    break;

                case 1:
                    ApplicationBar = ((ApplicationBar)Resources["AppBar2"]);

                    LoadTweets();

                    break;
            }
        }

#region controler

        private void refresh_Click(object sender, EventArgs e)
        {
            LoadTweets(true);

        }

        private void new_Tweet(object sender, EventArgs e)
        {
            if (Utils.LoadsettingsAsInt(Utils.IsConnected) == 0)
            {
                MessageBox.Show("You should Connect first to be able to tweet", "Mbomboh", MessageBoxButton.OK);
                return;
            }
            MessageBox.Show("Le ndjoh calle au coup! tu voulais meme deja tweeter hein!", "Ca ne waka pas encore", MessageBoxButton.OK);
        }

        private void SignInButton_OnTap(object sender, GestureEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/AuthorizationPage.xaml", UriKind.Relative));
            _viewModel.IsLoading = true;
            _viewModel.ProgressText = "Authenticating...";
            loginPopup = new Popup();
            var control = new AuthControl();
            control.FailledLogin = () =>
            {
                MessageBox.Show("An error occured! try to reload!", "Mollah", MessageBoxButton.OK);
                _viewModel.IsLoading = false;
                loginPopup.IsOpen = false;
            };
            control.SuccessfullLogin = () =>
            {
                _viewModel.IsLoading = false;
                loginPopup.IsOpen = false;
                LoadTweets();
            };
            loginPopup.Child = control;
            loginPopup.IsOpen = true;
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            if (loginPopup != null && loginPopup.IsOpen)
            {
                loginPopup.IsOpen = false;
                e.Cancel = true;
            }
        }

        private void Logout(object sender, EventArgs e)
        {
            Utils.UpdateSettings(Utils.IsConnected, 0);
            Utils.UpdateSettings(Utils.Key, "");
            Utils.UpdateSettings(Utils.Secret, "");
            _viewModel.RefreshList();
        }

        private void search_onTap(object sender, EventArgs e)
        {
            if (Utils.LoadsettingsAsInt(Utils.IsConnected) == 0)
            {
                MessageBox.Show("You should Connect first to be able to tweet", "Mbomboh", MessageBoxButton.OK);
                return;
            }
            MessageBox.Show("Le ndjooh calle au coup! tu voulais meme deja tweeter hein!", "Ca ne waka pas encore", MessageBoxButton.OK);
            return;
            //loginPopup = new Popup();
            //var control = new SearchControl();

            //control.Callback = (b, s) =>
            //{
            //    if (b)
            //    {
            //        SearchTweet(s);
            //    }
            //    loginPopup.IsOpen = false;

            //};
            //loginPopup.Child = control;
            //loginPopup.IsOpen = true;
        }
    }
#endregion
}
