﻿using System.Windows;
using System.Windows.Controls;

namespace NdoleRadio
{
    public partial class OverlayControl : UserControl
    {

        public string LoadingText
        {
            get { return GetValue(LoadingTextProperty) as string; }
            set{SetValue(LoadingTextProperty,value);}
        }

        public OverlayControl()
        {
            InitializeComponent();
            Loaded += OverlayControl_Loaded;
            
         //   LayoutRoot.Height = Application.Current.Host.Content.ActualHeight;
         //   LayoutRoot.Width = Application.Current.Host.Content.ActualWidth;   
        }

        void OverlayControl_Loaded(object sender, RoutedEventArgs e)
        {
            LayoutRoot.DataContext = this;
        }

        public static readonly DependencyProperty LoadingTextProperty = DependencyProperty.Register(
                                          "LoadingText",
                                          typeof(string),
                                          typeof(OverlayControl),
                                          new PropertyMetadata(""));
    }

    
}
