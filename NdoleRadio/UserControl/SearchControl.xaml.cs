﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace NdoleRadio
{
    public partial class SearchControl : UserControl
    {
        public Action<Boolean,string> Callback;
        public SearchControl()
        {
            InitializeComponent();
        }

        private void Cancel_on_tap(object sender, GestureEventArgs e)
        {
            if (Callback != null) Dispatcher.BeginInvoke(()=>Callback.Invoke(false,null));
        }

        private void Confirm_on_tap(object sender, GestureEventArgs e)
        {
            if(string.IsNullOrEmpty(PinTextBox.Text))return;
            if (Callback != null) Dispatcher.BeginInvoke(() => Callback.Invoke(true,PinTextBox.Text));
        }
    }
}

