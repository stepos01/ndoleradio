﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using TweetSharp;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace NdoleRadio
{
    public partial class AuthControl : UserControl
    {
       
         private TwitterService _service;
        private OAuthRequestToken _token;
        public Action SuccessfullLogin;
        public Action FailledLogin;
        public bool IsReset { get; set; }
        public AuthControl()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            LayoutRoot.Height = Application.Current.Host.Content.ActualHeight;
            LayoutRoot.Width = Application.Current.Host.Content.ActualWidth; 
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Login();
        }

        public void Login()
        {
            try
            {
                _service = new TwitterService("r7AnCK1m3e49PxUdZFGxCEGLn", "DiVcVdF1LdLUlpWzRZzNPGnO1s1EtBgE4eVs9HN42IR41Ch0ak");

                // Step 1 - Retrieve an OAuth Request Token
                _service.GetRequestToken("oob",(token, response) =>
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        _token = token;
                        Uri uri = _service.GetAuthorizationUri(token, "http://www.ndoleradio.com");
                        //Dispatcher.BeginInvoke(() => miniBrowser.Navigate(uri));
                        if (IsReset) return;
                        WebBrowserTask webBrowserTask = new WebBrowserTask();

                        webBrowserTask.Uri = uri;

                        webBrowserTask.Show();
                    }
                    else
                    {
                        if (FailledLogin != null) Dispatcher.BeginInvoke(FailledLogin.Invoke);
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                if (FailledLogin != null) Dispatcher.BeginInvoke(FailledLogin.Invoke);

            }

            // Step 2 - Redirect to the OAuth Authorization URL

           
        }

        public void Confirm_on_tap(object sender, GestureEventArgs e)
        {
            try
            {
                // Step 3 - Exchange the Request Token for an Access Token
                string verifier = PinTextBox.Text; // <-- This is input into your application by your user
                if (string.IsNullOrEmpty(verifier))
                {
                    MessageBox.Show("Login and insert the pin code or try to reload!", "Mollah", MessageBoxButton.OK);
                    return;
                }
                _service.GetAccessToken(_token, verifier, (token, response) =>
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        // Step 4 - User authenticates using the Access Token
                        _service.AuthenticateWith(token.Token, token.TokenSecret);
                        Utils.UpdateSettings(Utils.IsConnected,1);
                        Utils.UpdateSettings(Utils.Key,token.Token);
                        Utils.UpdateSettings(Utils.Secret,token.TokenSecret);
                        if(SuccessfullLogin!=null)Dispatcher.BeginInvoke(SuccessfullLogin.Invoke);
                    }
                    else
                    {
                        if (FailledLogin != null) Dispatcher.BeginInvoke(FailledLogin.Invoke);

                    }
                });
            }
            catch (Exception ex)
            {
                
                Debug.WriteLine(ex);
                if (FailledLogin != null) Dispatcher.BeginInvoke(FailledLogin.Invoke);
            }

          
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            Login();
        }


        private void UIElement_OnTap(object sender, GestureEventArgs e)
        {
            IsReset = true;
            if (FailledLogin != null) Dispatcher.BeginInvoke(FailledLogin.Invoke);
        }
    }
}
