﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using NdoleRadio.Annotations;
using TweetSharp;

namespace NdoleRadio
{
    class TwitterViewModel : INotifyPropertyChanged
    {
        private TwitterService _service;
        public event PropertyChangedEventHandler PropertyChanged;
        private bool _isLoading;

        public bool IsLoading
        {
            get { return _isLoading; }
            set { _isLoading = value; OnPropertyChanged(); }
        }
        private string defaultProgressText = "Loading new tweets...";
        private string _progressText = "";

        public string ProgressText
        {
            get { return _progressText; }
            set
            {
                _progressText = value;
                OnPropertyChanged();
            }
        }

        public void RefreshList()
        {
            OnPropertyChanged("IsButtonVisible");
            OnPropertyChanged("IsListVisible");
        }
        public Visibility IsListVisible
        {
            get
            {
                if (Utils.LoadsettingsAsInt(Utils.IsConnected) == 1) return Visibility.Visible;
                else return Visibility.Collapsed;
            }
        }

        public Visibility IsButtonVisible
        {
            get
            {
                if (Utils.LoadsettingsAsInt(Utils.IsConnected) == 0) return Visibility.Visible;
                else return Visibility.Collapsed;
            }
        }
        public TwitterViewModel()
        {
            TwitterCollection = new ObservableCollection<TwitterStatus>();
            _service = new TwitterService("r7AnCK1m3e49PxUdZFGxCEGLn", "DiVcVdF1LdLUlpWzRZzNPGnO1s1EtBgE4eVs9HN42IR41Ch0ak");

        }
        public ObservableCollection<TwitterStatus> TwitterCollection { get; set; }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void LoadTweets(Action<Boolean> callback, Boolean shouldRefresh = false)
        {
            RefreshList();

            if (Utils.LoadsettingsAsInt(Utils.IsConnected) == 0) return;
            if (shouldRefresh == true) TwitterCollection.Clear();
            long? maxId = null;
            if (TwitterCollection.Count > 0)
            {
                maxId = TwitterCollection.Last().Id;
            }
            ProgressText = defaultProgressText;
            IsLoading = true;
            _service.AuthenticateWith(Utils.LoadSettingsAsString(Utils.Key), Utils.LoadSettingsAsString(Utils.Secret));
            //service.Search(new SearchOptions() {Q = "#ndoleradio OR #NdoleRadio OR #ndolèradio",Count = 20,MaxId = maxId}, action);
            _service.ListTweetsOnUserTimeline(new ListTweetsOnUserTimelineOptions() { ScreenName = "NdoleRadio", MaxId = maxId },
                (statuses, response) =>
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() => IsLoading = false);
                        Deployment.Current.Dispatcher.BeginInvoke(() => statuses.ToList().ForEach(status => TwitterCollection.Add(status)));
                        callback.Invoke(true);
                    }
                    else
                    {
                        callback.Invoke(false);
                    }
                });
        }


        public void SearchTweet(string query,Action<Boolean>callbackAction)
        {
            
        }
    }
}
