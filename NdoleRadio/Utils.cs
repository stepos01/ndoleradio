﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;

namespace NdoleRadio
{
    static class Utils
    {
        public  const string IsConnected = "isConnected";
        public const string Key = "Key";
        public const string Secret = "Secret";
        public static string LoadSettingsAsString(String key)
        {
            var value = String.Empty;
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                value = IsolatedStorageSettings.ApplicationSettings[key] as string;
            }
            return value;
        }
       

        public static int LoadsettingsAsInt(string key)
        {
            int value = 0;
            if (!String.IsNullOrEmpty(LoadSettingsAsString(key)))
            {
                value = Int32.Parse(LoadSettingsAsString(key));
            }
            return value;

        }

       

      

        public static void UpdateSettings(String settings, string value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(settings))
            {
                IsolatedStorageSettings.ApplicationSettings[settings] = value;
            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings.Add(settings, value);
            }
            IsolatedStorageSettings.ApplicationSettings.Save();

        }

        public static void UpdateSettings(string settings, int value)
        {
            UpdateSettings(settings,Convert.ToString(value));

        }
      
    }
}
